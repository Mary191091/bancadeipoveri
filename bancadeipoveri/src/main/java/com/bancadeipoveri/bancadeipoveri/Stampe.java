package com.bancadeipoveri.bancadeipoveri;

import java.io.IOException;

import javax.print.PrintService;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.encoding.WinAnsiEncoding;

public class Stampe extends WinAnsiEncoding{

public void stampaMovimento(Movimenti movimento) {
	
	PDDocument doc =  new PDDocument();
	PDPage paginaBon = new PDPage();
	doc.addPage(paginaBon);
	
	try {
		PDPageContentStream cont = new PDPageContentStream 
				(doc, paginaBon);
	
		cont.beginText();
		cont.setLeading(20.3f);
		cont.newLineAtOffset(25, 700);
		
		String line1 =  "Banca dei Poveri: decostruita intorno a te";
		cont.setFont(PDType1Font.COURIER_BOLD_OBLIQUE, 18.30f);
		cont.showText(line1);
		cont.newLine();
		
		String line2 = movimento.getAccount().getNome().concat(movimento.getAccount().getCognome());
		cont.setFont(PDType1Font.TIMES_ROMAN, 12.3f);
		cont.showText(pulisciStringa(line2));
		cont.newLine();
		
		String tipoMovimento;
		if (movimento.isTipo()) {
			tipoMovimento = "Versamento";
		} else {
			tipoMovimento = "Prelievo";
		}
		String line3 = tipoMovimento 
				+ movimento.getCausale(); 
		cont.showText(pulisciStringa(line3));
		cont.newLine();
	
		String line4 = movimento.getData().toString();
		cont.showText(pulisciStringa(line4));
		cont.newLine();
		
		cont.endText();
		cont.close();
		doc.save("C:/Users/mariateresa.amatulli/Desktop/JAVA/piddieffe.pdf");
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

}
	

public String pulisciStringa(String stringaPul) {
	StringBuilder b = new StringBuilder();
	for (int i=0; i< stringaPul.length(); i++) {
		if (WinAnsiEncoding.INSTANCE.contains(stringaPul.charAt(i)))
			b.append(stringaPul.charAt(i));
		
	} return b.toString();
}


}
