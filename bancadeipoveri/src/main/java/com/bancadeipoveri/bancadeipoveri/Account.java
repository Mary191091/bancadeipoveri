package com.bancadeipoveri.bancadeipoveri;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//JAVA FUCK YOU

@Entity
@Table(name="account", uniqueConstraints = {
		@UniqueConstraint(columnNames = "idAccount")
})

public class Account {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAccount;
	private String nome;
	private String cognome;
	private double bilancio;
	private LocalDateTime dataUltimaModifica;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Movimenti> listaMovimenti;
	
	@OneToOne(mappedBy = "account")
	private Portfolio portfolio;
 
	
public Account() {
 }


public Integer getIdAccount() {
	return idAccount;
}


public void setIdAccount(Integer idAccount) {
	this.idAccount = idAccount;
}


public String getNome() {
	return nome;
}


public void setNome(String nome) {
	this.nome = nome;
}


public String getCognome() {
	return cognome;
}


public void setCognome(String cognome) {
	this.cognome = cognome;
}


public double getBilancio() {
	return bilancio;
}


public void setBilancio(double bilancio) {
	this.bilancio = bilancio;
}


public LocalDateTime getDataUltimaModifica() {
	return dataUltimaModifica;
}


public void setDataUltimaModifica(LocalDateTime dataUltimaModifica) {
	this.dataUltimaModifica = dataUltimaModifica;
}


public List<Movimenti> getListaMovimenti() {
	return listaMovimenti;
}


public void setListaMovimenti(List<Movimenti> listaMovimenti) {
	this.listaMovimenti = listaMovimenti;
}


protected Portfolio getPortfolio() {
	return portfolio;
}


protected void setPortfolio(Portfolio portfolio) {
	this.portfolio = portfolio;
}


@Override
public String toString() {
	return "Account [idAccount=" + idAccount + ", nome=" + nome + ", cognome=" + cognome + ", bilancio=" + bilancio
			+ ", dataUltimaModifica=" + dataUltimaModifica + ", listaMovimenti=" + listaMovimenti + "]";
}






}