package com.bancadeipoveri.bancadeipoveri;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BancaRepository extends CrudRepository<Account, Integer>{
//List<Movimenti> findByIdMovimenti(int id_account);
	
    void save(List<Movimenti> listaMov);	
	Account findById(int id_account);
	public List<Account> findAll();


	

	
	
	
}
