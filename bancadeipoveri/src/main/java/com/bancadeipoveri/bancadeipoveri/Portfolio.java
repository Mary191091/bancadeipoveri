package com.bancadeipoveri.bancadeipoveri;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Portfolio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_portfolio; 
	private double totalValue;
	private double rendimentoAnnuo;
	private LocalDateTime data_inserimento;
	
	
	protected LocalDateTime getData_inserimento() {
		return data_inserimento;
	}

	protected void setData_inserimento(LocalDateTime data_inserimento) {
		this.data_inserimento = data_inserimento;
	}

	@OneToOne
	@JoinColumn(name = "id_account")
	private Account account;
	
	@ManyToMany(cascade = { CascadeType.ALL})
	@JoinTable(
			name = "portfolio_titoli",
			joinColumns = { @JoinColumn(name = "portfolio_id")},
			inverseJoinColumns = {@JoinColumn(name="titolo_id")
			})
	private List<Titolo> pacchettoTitoli;

	protected int getId_portfolio() {
		return id_portfolio;
	}

	protected void setId_portfolio(int id_portfolio) {
		this.id_portfolio = id_portfolio;
	}

	protected double getTotalValue() {
		return totalValue;
	}

	protected void setTotalValue(double totalValue) {
		this.totalValue = totalValue;
	}

	protected double getRendimentoAnnuo() {
		return rendimentoAnnuo;
	}

	protected void setRendimentoAnnuo(double rendimentoAnnuo) {
		this.rendimentoAnnuo = rendimentoAnnuo;
	}

	protected Account getAccount() {
		return account;
	}

	protected void setAccount(Account account) {
		this.account = account;
	}

	protected List<Titolo> getPacchettoTitoli() {
		return pacchettoTitoli;
	}

	protected void setPacchettoTitoli(List<Titolo> pacchettoTitoli) {
		this.pacchettoTitoli = pacchettoTitoli;
	}

	@Override
	public String toString() {
		return "Portfolio [id_portfolio=" + id_portfolio + ", totalValue=" + totalValue + ", rendimentoAnnuo="
				+ rendimentoAnnuo + ", data_inserimento=" + data_inserimento + ", account=" + account
				+ ", pacchettoTitoli=" + pacchettoTitoli + "]";
	}

	
	
	
}
