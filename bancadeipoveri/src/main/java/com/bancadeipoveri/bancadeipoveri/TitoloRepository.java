package com.bancadeipoveri.bancadeipoveri;

import org.springframework.data.repository.CrudRepository;

public interface TitoloRepository extends CrudRepository<Titolo, Integer>{

	
}
