package com.bancadeipoveri.bancadeipoveri;

public enum Valuta {
    
    AED("United Arab Emirates Dirham", 0.345),
    AFN("Afghanistan Afghani", 0.546),
    ALL("Albania Lek", 0.80),
    AMD("Armenia Dram", 0.15),
    ANG("Netherlands Antilles Guilder", 1.23),
    AOA("Angola Kwanza", 0.12),
	EUR("European euro", 1.00);

    private String description;
	private double cambioMonetaEuro; 
	//N.B. 1 AOA = 0.12 EURO 

    Valuta(String description, double cambioMonetaEuro) {
        this.description = description;
        this.cambioMonetaEuro = cambioMonetaEuro;
    }

    public String getDescription() {
        return description;
    }
	
	public double getCambioMonetaEuro() {
		return cambioMonetaEuro;
	}

	public void setCambioMonetaEuro(double cambioMonetaEuro) {
		this.cambioMonetaEuro = cambioMonetaEuro;
	}

	public void setCambioMonetaEuro(float cambioMonetaEuro) {
		this.cambioMonetaEuro = cambioMonetaEuro;
	}


    
}
