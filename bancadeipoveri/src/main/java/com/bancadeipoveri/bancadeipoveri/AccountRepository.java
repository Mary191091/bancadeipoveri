package com.bancadeipoveri.bancadeipoveri;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Integer> {

public List<Account> findAll();
public Account findById(int idAccount);

//public Movimenti findByMovimentiCausale();
//esempio è 
//List<Person> findByAddressZipCode(ZipCode zipCode);

//List<Account> findByPortfolioTitolo(Titolo titolo);

} 