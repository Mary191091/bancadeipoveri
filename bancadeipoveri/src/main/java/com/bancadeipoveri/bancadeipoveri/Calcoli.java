package com.bancadeipoveri.bancadeipoveri;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class Calcoli {

	public double operazioneBilancio(Movimenti movimento, Account account) {
		
		double bilancio = account.getBilancio();
		
		if (movimento.isTipo() == true) {
			bilancio += movimento.getAmmontare();		
			
		} else if ((movimento.isTipo() == false) && (bilancio < movimento.getAmmontare())){	
			System.out.println("Account in rosso. Ritirabile massimo" + bilancio);
		} else if (movimento.isTipo() == false){
			
			bilancio -= movimento.getAmmontare();
		}
		
		account.setDataUltimaModifica(LocalDateTime.now()); 
		return bilancio;
	} 
	
	
	public double operazioneEstero(Movimenti movEstero, Account account) {
		
		double bilancio = account.getBilancio();
		double cambio = movEstero.getValuta().getCambioMonetaEuro() * movEstero.getAmmontare();
		Stampe stampe = new Stampe();
		if (movEstero.isTipo() == true) {
	
			bilancio += cambio;
			
			
		} else if ((movEstero.isTipo() == false) && (bilancio < cambio)){
			System.out.println("Account in rosso.Puoi ritirare massimo" + bilancio);
	} else {
		bilancio -= cambio;
	}
		
		stampe.stampaMovimento(movEstero);
		return bilancio;
		
	}
	
	public Portfolio commissioneAperturaPortfolio(Portfolio portfolio) {
		double commissione = 0;
		double rendimentoAnnuoPortfolio = 0;
		List<Titolo> listaTitoli = portfolio.getPacchettoTitoli();
		
		for (Titolo titolo : listaTitoli) {
		rendimentoAnnuoPortfolio += titolo.getRendimento_annuo();
		}
		commissione = (rendimentoAnnuoPortfolio * 0.012) + (portfolio.getTotalValue() * 0.005);
		
		portfolio.setTotalValue(portfolio.getTotalValue() - commissione);
		
		return portfolio;
	}
	
	
}
