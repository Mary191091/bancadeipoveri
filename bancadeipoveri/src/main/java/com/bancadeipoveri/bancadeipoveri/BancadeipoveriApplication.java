package com.bancadeipoveri.bancadeipoveri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancadeipoveriApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancadeipoveriApplication.class, args);
	}
	
	 
}
