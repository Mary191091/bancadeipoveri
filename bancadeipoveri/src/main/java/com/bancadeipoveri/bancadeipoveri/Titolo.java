package com.bancadeipoveri.bancadeipoveri;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Titolo {

		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE)
		private int id_titolo;
		private String nome_titolo;
		private double rendimento_annuo;
		private double frazione;
		private boolean immobiliare;
		private double percent_tax_annua;
		private LocalDateTime data_inserimento;
		
		@ManyToMany(fetch= FetchType.LAZY)
		@JoinColumn(name="id_portfolio", nullable=false)
		private List<Portfolio> portfolio;

 
		protected int getId_titolo() {
			return id_titolo;
		}


		protected LocalDateTime getData_inserimento() {
			return data_inserimento;
		}


		protected void setData_inserimento(LocalDateTime data_inserimento) {
			this.data_inserimento = data_inserimento;
		}


		protected void setId_titolo(int id_titolo) {
			this.id_titolo = id_titolo;
		}


		protected String getNome_titolo() {
			return nome_titolo;
		}


		protected void setNome_titolo(String nome_titolo) {
			this.nome_titolo = nome_titolo;
		}


		protected double getRendimento_annuo() {
			return rendimento_annuo;
		}


		protected void setRendimento_annuo(double rendimento_annuo) {
			this.rendimento_annuo = rendimento_annuo;
		}


		protected double getFrazione() {
			return frazione;
		}


		protected void setFrazione(double frazione) {
			this.frazione = frazione;
		}


		protected boolean isImmobiliare() {
			return immobiliare;
		}


		protected void setImmobiliare(boolean immobiliare) {
			this.immobiliare = immobiliare;
		}


		protected double getPercent_tax_annua() {
			return percent_tax_annua;
		}


		protected void setPercent_tax_annua(double percent_tax_annua) {
			this.percent_tax_annua = percent_tax_annua;
		}


		protected List<Portfolio> getPortfolio() {
			return portfolio;
		}


		protected void setPortfolio(List<Portfolio> portfolio) {
			this.portfolio = portfolio;
		}


		


		
		
		
}
