package com.bancadeipoveri.bancadeipoveri;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;

@RestController
public class BancaController {

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private MovimentoRepository movimentoRepository;
	@Autowired
	private PortfolioRepository portfolioRepository;
	@Autowired
	private TitoloRepository titoloRepository;

	//<------------------- METODI PER GESTIONE ACCOUNT ---------------------------------->

	@GetMapping("/account")
	public List<Account> getAll() {
		List<Account> listaAccount = accountRepository.findAll();
		return listaAccount;

	}

	@GetMapping("/account/{id}")
	public Account accountById(@PathVariable("id") int idAccount) {
		Account account = accountRepository.findById(idAccount);
		return account;
	}

	@PostMapping("/account")
	@ResponseStatus(HttpStatus.CREATED)

	public void creaAccount(@RequestBody Account account) {
		account.setDataUltimaModifica(LocalDateTime.now());
		accountRepository.save(account);
	}
	
	@DeleteMapping("/account/{id}") //FUNZIONA
	public void eliminaAccount(@PathVariable("id") int id_account) {
		accountRepository.deleteById(id_account);
	}
	
	@PutMapping("/account/{id}") //funziona
	public void modificaAccount(@PathVariable("id") int id_account, @RequestBody Account accountEntrata) {
		accountEntrata.setIdAccount(id_account);
		accountEntrata.setDataUltimaModifica(LocalDateTime.now());
		accountRepository.save(accountEntrata);
		
	}
	
	@PatchMapping("/account/{id}") //FUNZIONA (escono le graffe, se fatto con double in @Req body, JSON PARSER/DESERIALIZER PROBLEM, NON RIESCE A DESERIALIZZARE IL DOUBLE)
	public void modificaParteAccount(@PathVariable("id") int id_account, @RequestBody String nuovoNome) {
		accountRepository.findById(id_account).setDataUltimaModifica(LocalDateTime.now());
		Account account = accountRepository.findById(id_account);
		account.setNome(nuovoNome);
		accountRepository.save(account);
		
		
	}
	

//	<----------------METODI PER GESTIRE I MOVIMENTI (ACCREDITI, PRELIEVI, ESTERI/UE) ----------------->

	@PostMapping("/account/{id}/movements")
	@ResponseStatus(HttpStatus.CREATED)
	public void nuovoMovimento(@PathVariable("id") int id_account, @RequestBody Movimenti movimento) {

		Account account = accountRepository.findById(id_account);
		movimento.setAccount(account);
		movimento.setData(LocalDateTime.now());
		Calcoli calcolo = new Calcoli();

		if (movimento.getValuta().toString() == "EUR") {
			account.setBilancio(calcolo.operazioneBilancio(movimento, account));
			movimento = movimentoRepository.save(movimento);
			accountRepository.save(account);
			Stampe stampe = new Stampe();
			stampe.stampaMovimento(movimento);
		} else {
			account.setBilancio(calcolo.operazioneEstero(movimento, account));
			movimento = movimentoRepository.save(movimento);
			accountRepository.save(account);
			Stampe stampe = new Stampe();
			stampe.stampaMovimento(movimento);
		}

	}

	@GetMapping("/account/{id}/movements")
	public List<Movimenti> getMovimenti(@PathVariable("id") int id_account) {
		List<Movimenti> listaMovimenti = accountRepository.findById(id_account).getListaMovimenti();
		return listaMovimenti;

	}
	
	
	@PutMapping("/account/{id}/movements")
	public void amendMovement(@PathVariable("id") int id_account, @RequestBody Movimenti movimento){
	
		if (movimentoRepository.existsById(movimento.getIdMovimento())) {
			movimentoRepository.deleteById(movimento.getIdMovimento());
			movimento.setAccount((accountRepository.findById(id_account)));
			movimento.setData(LocalDateTime.now());
			movimentoRepository.save(movimento);
		} else {
			movimento.setData(LocalDateTime.now());
			movimentoRepository.save(movimento);
		} 
	}
	
	
	@PatchMapping("/account/{id}/movements")
	public void amendPartialMovement(@PathVariable("id") int id_account, 
			@RequestParam(value="id_movimento", required=true) int id_movimento,
		@RequestBody String causaleEntrata) {
//		(@RequestParam(value="priorita", required=false) String priorita, @RequestParam(value="categoria", required=false) String categoria)
		Movimenti movimento =  movimentoRepository.findById(id_movimento).get();
		movimento.setData(LocalDateTime.now());
		movimentoRepository.findById(id_movimento).get().setCausale(causaleEntrata);
		movimentoRepository.save(movimento);

		

	}
	
	
	/*
	 * <-------------------- METODI RIGUARDANTI LA GESTIONE DEL PORTFOLIO ---------------------------------------->
	 */

	//GET DA 200 OK ma non viene restituito nulla dall'interrogazione. Metodo testato, in console stampa tutto tutto 
	@GetMapping("/account/{id}/portfolio")
	public void getPortfolio(@PathVariable("id") int id_account) {
		Portfolio portfolio = accountRepository.findById(id_account).getPortfolio();
		System.out.println(portfolio);
		
	}


	@PostMapping("/account/{id}/portfolio") //FUNZIONA - inserire i titoli a mano
	public void createPortfolio(@PathVariable("id") int id_account, @RequestBody Portfolio portfolio) {
		portfolio.setAccount(accountRepository.findById(id_account));
		Calcoli calcolo = new Calcoli();
		calcolo.commissioneAperturaPortfolio(portfolio);
		portfolio.setData_inserimento(LocalDateTime.now());
		portfolioRepository.save(portfolio);
		Optional<Portfolio> titoloOptional = 
				portfolioRepository.findById(portfolio.getId_portfolio());

	}

	
    @PatchMapping("/account/{id}/portfolio") //DA CHIEDERE
    public void modificaRedimentoPortfolio (@PathVariable ("id") int id_account, @RequestBody int id_portfolio, double rendimentoAnnuo) {
    	Portfolio port = portfolioRepository.findById(id_portfolio).get();
    	 port.setRendimentoAnnuo(rendimentoAnnuo);
    	 portfolioRepository.save(port);
     	port.setRendimentoAnnuo(rendimentoAnnuo);
    	portfolioRepository.save(port);

    }
    
    
    
    @DeleteMapping ("/account/{id}/portfolio/{id_portfolio}")
    public void eliminaPortfolio(@PathVariable("id") int id_account, @PathVariable("id") int id_portfolio){
    	portfolioRepository.deleteById(id_portfolio);
    	
    	
    }
	
    @PutMapping ("/account/{id}/portfolio")
    public void modificaPortfolio(@PathVariable ("id") int id_account, @RequestBody Portfolio portfolioEntrata) {
    	portfolioRepository.save(portfolioEntrata);
    }
    
	
	
	
	// <------------------ METODI PER LA GESTIONE DEI TITOLI [NEL PORTFOLIO]--------------------------------------->

	
	@GetMapping ("/account/{id}/portfolio/{id}/titolo")
	public Titolo getTitolo (@PathVariable ("id") int id_account, @PathVariable ("id") int id_portfolio) {
		Titolo titolo = titoloRepository.findById(id_portfolio).get();
		return titolo;
		
	}
	
	@PostMapping ("/account/{id}/portfolio/{id}/titolo")
	public void creaTitolo (@PathVariable("id") int id_account, @PathVariable("id") int id_portfolio, @RequestBody Titolo nuovoTitolo) {
		//portfolio.setAccount(accountRepository.findById(id_account));
		
	}
	
	
	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
