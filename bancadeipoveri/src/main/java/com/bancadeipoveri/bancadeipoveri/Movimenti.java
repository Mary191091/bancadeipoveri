package com.bancadeipoveri.bancadeipoveri;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Movimenti {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idMovimento;
	private double ammontare;
	private boolean tipo;
	private String causale;
	private LocalDateTime data;
	private Valuta valuta;
	//private Commissione commissione;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idAccount", nullable = false)
	private Account account;

	protected Integer getIdMovimento() {
		return idMovimento;
	}

	protected void setIdMovimento(Integer idMovimento) {
		this.idMovimento = idMovimento;
	}

	protected double getAmmontare() {
		return ammontare;
	}

	protected void setAmmontare(double ammontare) {
		this.ammontare = ammontare;
	}

	protected boolean isTipo() {
		return tipo;
	}

	protected void setTipo(boolean tipo) {
		this.tipo = tipo;
	}

	protected String getCausale() {
		return causale;
	}

	protected void setCausale(String causale) {
		this.causale = causale;
	}

	protected LocalDateTime getData() {
		return data;
	}

	protected void setData(LocalDateTime data) {
		this.data = data;
	}

	protected Valuta getValuta() {
		return valuta;
	}

	protected void setValuta(Valuta valuta) {
		this.valuta = valuta;
	}

	protected Account getAccount() {
		return account;
	}

	protected void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "Movimenti [idMovimento=" + idMovimento + ", ammontare=" + ammontare + ", tipo=" + tipo + ", causale="
				+ causale + ", data=" + data + ", valuta=" + valuta + ", account=" + account + "]";
	}
	

	
} 